import expressLoader from "./express";
import dependencyInjectorLoader from "./depInjector";
import mongooseLoader from "./mongoose";
import Logger from "./logger";
import "./events";

export default async ({ expressApp }) => {
  const mongoConnection = await mongooseLoader();
  Logger.info("DB connected!");

  const userModel = {
    name: "userModel",
    model: require("../models/user").default
  };

  const taskModel = {
    name: "taskModel",
    model: require("../models/task").default
  };

  await dependencyInjectorLoader({
    mongoConnection,
    models: [userModel, taskModel]
  });
  Logger.info("Dependency Injector loaded");

  await expressLoader({ app: expressApp });
  Logger.info("Express loaded");
};
