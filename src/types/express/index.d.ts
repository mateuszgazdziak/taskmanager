import { Document, Model } from "mongoose";
import { IUser } from "../../interfaces/IUser";
import { ITask } from "../../interfaces/ITask";
declare global {
  namespace Express {
    export interface Request {
      currentUser: IUser & Document;
    }
  }

  namespace Models {
    export type UserModel = Model<IUser & Document>;
    export type TaskModel = Model<ITask & Document>;
  }
}
