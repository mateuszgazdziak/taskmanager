import { Service, Inject } from "typedi";
import {
  ITaskInputDTO,
  ITask,
  ITaskEditDTO,
  ITaskDeleteDTO
} from "../interfaces/ITask";

@Service()
export default class TaskService {
  constructor(
    @Inject("taskModel") private taskModel: Models.TaskModel,
    @Inject("logger") private logger
  ) {}

  public async ggetAllTaskset(): Promise<{ task: ITask }> {
    try {
      const taskRecord = await this.taskModel.find();
      return taskRecord;
    } catch (e) {
      this.logger.error(e);
      throw e;
    }
  }

  public async addNewTask(
    taskInputDTO: ITaskInputDTO
  ): Promise<{ task: ITask }> {
    try {
      const records = await this.taskModel.create({ ...taskInputDTO });
      return records;
    } catch (e) {
      this.logger.error(e);
      throw e;
    }
  }

  public async editTask(taskInputDTO: ITaskEditDTO): Promise<{ task: ITask }> {
    try {
      const {
        id,
        shouldBeDoneTimestamp,
        shouldBeRemidedTimestamp,
        title,
        description
      } = taskInputDTO;
      const taskRecord = await this.taskModel.findOneAndUpdate(
        { _id: id },
        { shouldBeDoneTimestamp, shouldBeRemidedTimestamp, title, description },
        { new: true }
      );

      return taskRecord;
    } catch (e) {
      this.logger.error(e);
      throw e;
    }
  }

  public async deleteTask(taskInputDTO: ITaskDeleteDTO): Promise<any> {
    try {
      const { id } = taskInputDTO;
      await this.taskModel.remove({ _id: id });
    } catch (e) {
      this.logger.error(e);
      throw e;
    }
  }
}
