import { ITask } from "../interfaces/ITask";
import mongoose from "mongoose";

const Task = new mongoose.Schema(
  {
    title: {
      type: String,
      required: [true, "Please enter title"]
    },

    description: {
      type: String
    },

    shouldBeDoneTimestamp: Number,

    shouldBeRemidedTimestamp: Number
  },
  { timestamps: true }
);

export default mongoose.model<ITask & mongoose.Document>("Task", Task);
