import { Router, Request, Response } from "express";
import middlewares from "../middlewares";
import { celebrate, Joi } from "celebrate";
import Container from "typedi";
import TaskService from "../../services/task";
import {
  ITaskInputDTO,
  ITaskEditDTO,
  ITaskDeleteDTO
} from "../../interfaces/ITask";
import { Logger } from "winston";
import { NextFunction } from "connect";
const route = Router();

export default (app: Router) => {
  app.use("/tasks", route);

  route.get(
    "/",
    middlewares.isAuth,
    middlewares.attachCurrentUser,
    async (req: Request, res: Response, next: NextFunction) => {
      const logger: Logger = Container.get("logger");
      try {
        const taskServiceInstance = Container.get(TaskService);
        const result = await taskServiceInstance.ggetAllTaskset();

        return res.status(200).json(result);
      } catch (e) {
        logger.error("error: %o", e);
        return next(e);
      }
    }
  );

  route.post(
    "/new",
    celebrate({
      body: Joi.object({
        title: Joi.string().required(),
        description: Joi.string().required(),
        shouldBeDoneTimestamp: Joi.number(),
        shouldBeRemidedTimestamp: Joi.number()
      })
    }),
    middlewares.isAuth,
    middlewares.attachCurrentUser,
    async (req: Request, res: Response, next: NextFunction) => {
      const logger: Logger = Container.get("logger");
      try {
        const taskServiceInstance = Container.get(TaskService);
        const result = await taskServiceInstance.addNewTask(
          req.body as ITaskInputDTO
        );

        return res.status(201).json(result);
      } catch (e) {
        logger.error("error: %o", e);
        return next(e);
      }
    }
  );

  route.put(
    "/edit",
    celebrate({
      body: Joi.object({
        id: Joi.string().required(),
        title: Joi.string(),
        description: Joi.string(),
        shouldBeDoneTimestamp: Joi.number(),
        shouldBeRemidedTimestamp: Joi.number()
      })
    }),
    middlewares.isAuth,
    middlewares.attachCurrentUser,
    async (req: Request, res: Response, next: NextFunction) => {
      const logger: Logger = Container.get("logger");
      try {
        const taskServiceInstance = Container.get(TaskService);
        const result = await taskServiceInstance.editTask(
          req.body as ITaskEditDTO
        );

        return res.status(200).json(result);
      } catch (e) {
        logger.error("error: %o", e);
        return next(e);
      }
    }
  );

  route.delete(
    "/delete",
    celebrate({
      body: Joi.object({
        id: Joi.string().required()
      })
    }),
    middlewares.isAuth,
    middlewares.attachCurrentUser,
    async (req: Request, res: Response, next: NextFunction) => {
      const logger: Logger = Container.get("logger");
      try {
        const taskServiceInstance = Container.get(TaskService);
        const result = await taskServiceInstance.deleteTask(
          req.body as ITaskDeleteDTO
        );

        return res.status(200).json(result);
      } catch (e) {
        logger.error("error: %o", e);
        return next(e);
      }
    }
  );
};
