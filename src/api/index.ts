import { Router } from "express";
import auth from "./routes/auth";
import user from "./routes/user";
import tasks from "./routes/tasks";
export default () => {
  const app = Router();
  auth(app);
  user(app);
  tasks(app);

  return app;
};
