import attachCurrentUser from './setCurrentUser';
import isAuth from './isAuth';

export default {
  attachCurrentUser,
  isAuth,
};
