export interface IUser {
    _id: string;
    email: string;
    name: string;
    password: string;
  }
  
  export interface IUserInputDTO {
    name: string;
    email: string;
    password: string;
  }
  