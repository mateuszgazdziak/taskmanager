export interface ITask {
  _id: string;
  title: string;
  description: string;
  shouldBeDoneTimestamp: number;
  shouldBeRemidedTimestamp: number;
}

export interface ITaskInputDTO {
  title: string;
  description: string;
  shouldBeDoneTimestamp: number;
  shouldBeRemidedTimestamp: number;
}

export interface ITaskEditDTO {
  id: string;
  title: string;
  description: string;
  shouldBeDoneTimestamp: number;
  shouldBeRemidedTimestamp: number;
}

export interface ITaskDeleteDTO {
  id: string;
}
