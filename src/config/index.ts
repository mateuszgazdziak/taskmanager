import dotenv from "dotenv";

const isConfigLoaded = dotenv.config();

if (!isConfigLoaded) {
  throw new Error(".env file could not be lodaded");
}

export default {
  port: parseInt(process.env.PORT, 10) || 3000,
  databaseURL: process.env.MONGODB_URI,
  jwtSecret: process.env.JWT_SECRET,
  apiPrefix: "/api/v1",
  logs: {
    level: process.env.LOG_LEVEL || "silly"
  }
};
