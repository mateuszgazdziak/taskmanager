import supertest from "supertest";
import { startServer } from "../src/app";

let app;
let request;

const userCredentials = {
  email: "mateusz@mateusz.pl",
  password: "test"
};

let token;
let id;

beforeAll(async done => {
  app = await startServer();
  request = supertest(app);
  done();
});

beforeAll(function(done) {
  request
    .post("/api/v1/auth/signin")
    .send(userCredentials)
    .end(function(err, res) {
      token = res.body.token;
      done();
    });
});

// ==============================================
//  STATUS
// ==============================================

describe("/status", () => {
  describe("GET /status endpoint should be accessible", () => {
    it("should return 200", done => {
      request.get("/status").expect(200, done);
    }, 3000);
  });
});

// ==============================================
//  USERS
// ==============================================

describe("/api/v1/users", () => {
  describe("Requires authorization", () => {
    it("should return 401", done => {
      supertest(app)
        .get("/api/v1/users/info")
        .expect(401, done);
    });
  });
  describe("Works if user pass token", () => {
    it("should return 200", done => {
      supertest(app)
        .get("/api/v1/users/info")
        .set("Authorization", "Bearer " + token)
        .expect(200, done);
    });
  });
});

// ==============================================
//  TASKS
// ==============================================

describe("/api/v1/tasks/", () => {
  describe("Requires authorization", () => {
    it("should return 401", done => {
      supertest(app)
        .get("/api/v1/tasks")
        .expect(401, done);
    });
  });
  describe("Works if user pass token", () => {
    it("should return 200", done => {
      supertest(app)
        .get("/api/v1/tasks")
        .set("Authorization", "Bearer " + token)
        .expect(200, done);
    });
  });
});

describe("api/v1/tasks/new", () => {
  describe("Should create new task with passed title and descriptions", () => {
    it("should return 201", done => {
      supertest(app)
        .post("/api/v1/tasks/new")
        .set("Authorization", "Bearer " + token)
        .send({ title: "test", description: "testste" })
        .expect(201)
        .end(function(err, res) {
          id = res.body._id;
          done();
        });
    });
  });

  describe("Should fail if no all required params are passed - missing description", () => {
    it("should return 500", done => {
      supertest(app)
        .post("/api/v1/tasks/new")
        .set("Authorization", "Bearer " + token)
        .send({ title: "test" })
        .expect(500, done);
    });
  });

  describe("Should fail if no all required params are passed - missing title", () => {
    it("should return 500", done => {
      supertest(app)
        .post("/api/v1/tasks/new")
        .set("Authorization", "Bearer " + token)
        .send({ description: "test" })
        .expect(500, done);
    });
  });

  describe("Should fail if user pass wrong param", () => {
    it("should return 500", done => {
      supertest(app)
        .post("/api/v1/tasks/new")
        .set("Authorization", "Bearer " + token)
        .send({ description: "test", title: "oooo", wrongParam: "wrong" })
        .expect(500, done);
    });
  });

  describe("Creating requires authorization", () => {
    it("should return 401", done => {
      supertest(app)
        .post("/api/v1/tasks/new")
        .send({ title: "test", description: "testste" })
        .expect(401, done);
    });
  });

  describe("Should edit new task with passed params - all acceptable params", () => {
    it("should return 200", done => {
      supertest(app)
        .put("/api/v1/tasks/edit")
        .set("Authorization", "Bearer " + token)
        .send({
          id,
          title: "test",
          description: "testste",
          shouldBeDoneTimestamp: 333,
          shouldBeRemidedTimestamp: 777
        })
        .expect(200, done);
    });
  });

  describe("Should edit new task with passed params", () => {
    it("should return 200", done => {
      supertest(app)
        .put("/api/v1/tasks/edit")
        .set("Authorization", "Bearer " + token)
        .send({ id, title: "test", description: "testste" })
        .expect(200, done);
    });
  });

  describe("Editing requires authorization", () => {
    it("should return 401", done => {
      supertest(app)
        .put("/api/v1/tasks/edit")
        .send({ id: "test", title: "test", description: "testste" })
        .expect(401, done);
    });
  });

  describe("Editing requires passing id", () => {
    it("should return 500", done => {
      supertest(app)
        .put("/api/v1/tasks/edit")
        .set("Authorization", "Bearer " + token)
        .send({ title: "test", description: "testste" })
        .expect(500, done);
    });
  });

  describe("Should delete task by passed id", () => {
    it("should return 200", done => {
      supertest(app)
        .del("/api/v1/tasks/delete")
        .set("Authorization", "Bearer " + token)
        .send({ id })
        .expect(200, done);
    });
  });

  describe("Deleting requires authorization", () => {
    it("should return 401", done => {
      supertest(app)
        .del("/api/v1/tasks/delete")
        .send({ id: "test" })
        .expect(401, done);
    });
  });

  describe("Deleting requires passing id", () => {
    it("should return 500", done => {
      supertest(app)
        .del("/api/v1/tasks/delete")
        .send({ notid: "test" })
        .expect(500, done);
    });
  });
});
